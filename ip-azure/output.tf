output "id" {
  value = azurerm_public_ip.pubip.id
}

output "ip_id" {
  value = azurerm_public_ip.pubip.id
}

output "address" {
  value = azurerm_public_ip.pubip.ip_address
}

output "description" {
  value = var.description
}
